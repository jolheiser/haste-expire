# haste-expire

A utility to remove old [Hastebin](https://github.com/zneix/haste-server) files based on an expire time and last read date of the file.  

**haste-expire performs destructive operations and should therefore be ran as its own user!**

-----

## Instructions

1. Download haste-expire from a release (or build it yourself from source).
2. Move it anywhere you want.
3. Run it with the appropriate flags.

## Flags

`--interval` - A [duration](https://golang.org/pkg/time/#ParseDuration) between checks, similar to a cron.

`--single` - Run the scan once and stop. Useful for cron or systemd timers. 

`--expire` - How long before removing a file that hasn't been accessed in a [duration](https://golang.org/pkg/time/#ParseDuration).

`--data-path` - The path to your haste files.
