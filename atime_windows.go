package main

import (
	"os"
	"syscall"
	"time"
)

func LastAccess(info os.FileInfo) time.Time {
	return time.Unix(0, info.Sys().(*syscall.Win32FileAttributeData).LastAccessTime.Nanoseconds())
}
